const express = require("express");
const expresshb = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');

const db = require('./config/database');
const app = express();

const Sequelize = require('sequelize');

// Handlebars
app.engine('handlebars', expresshb({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use(express.static(path.join(__dirname,'public')));
const port = process.env.port || 5000;

// gig routes
app.use('/gigs', require('./routes/gigs'));
// support parsing of application/json type post data

app.get('/', (req,res)=>{
  res.send("Work");
});

app.listen(port, console.log(`Server started at port ${port}`))

